using System.Net;
using NUnit.Framework;
using Spottedo.Model.Navigation;
using Spottedo.Model.Requests.Update;
using Spottedo.Model.Responses.Update;

namespace Spottedo.Tests
{
    using System;
    using System.Collections.Generic;
    using Common.DTO;
    using Common.Utils;
    using Model.Messages;

    [TestFixture]
    public class NUnitTest
    {
        [Test]
        public void UpdateTest()
        {
            var isGotResp = false;
            var ur = new UpdateRequest
            {
                Count = 10,
                IgnoreMessages = new List<Message>(),
                Location = new Location(0, 0, 0, LocationSourceEnum.Geolocation),
                Since = Tools.ConvertToEpoch(DateTime.Now),
                StartFrom = 0
            };
            ur.NewResponse += (sender, response) =>
            {
                var updateResponse = (UpdateResponse)response;
                Assert.AreEqual(updateResponse.HttpCode, HttpStatusCode.OK);
                Assert.AreEqual(updateResponse.Messages.Count, 3);
                isGotResp = true;
            };
            ur.SendAsync();
            while (!isGotResp) {}
        }
    }
}
