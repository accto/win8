﻿namespace Spottedo.Model.Navigation
{
    interface INavigation
    {
        Location GetLocation();
    }
}
