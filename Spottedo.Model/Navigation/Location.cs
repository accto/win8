﻿namespace Spottedo.Model.Navigation
{
    using Common.DTO;

    public class Location
    {
        private int latitude;

        public int Latitude
        {
            get { return latitude; }
        }

        private int longtitude;

        public int Longtitude
        {
            get { return longtitude; }
        }

        private int precision;

        public int Precision
        {
            get { return precision; }
        }

        private LocationSourceEnum source;

        public LocationSourceEnum Source
        {
            get { return source; }
        }

        public Location(int latitude, int longtitude, int precision, LocationSourceEnum source)
        {
            this.latitude = latitude;
            this.longtitude = longtitude;
            this.precision = precision;
            this.source = source;
        }
    }
}
