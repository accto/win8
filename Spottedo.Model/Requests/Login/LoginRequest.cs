﻿using System;
using Newtonsoft.Json;
using Spottedo.Model.Responses;
using Spottedo.Model.Responses.Login;

namespace Spottedo.Model.Requests.Login
{
    using Common.DTO;

    public class LoginRequest : BaseRequest
    {
        public string Version { get; set; }
        public OsEnum Os { get; set; }
        public string Locale { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override async void SendAsync()
        {
            var body = JsonConvert.SerializeObject(this);
            var response = await SendHelper(@"login", body);

            var responseString = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            var loginResponse = !String.IsNullOrEmpty(responseString) ? JsonConvert.DeserializeObject<LoginResponse>(responseString) : new LoginResponse();

            loginResponse.HttpCode = response.StatusCode;
            if (NewResponse != null)
                NewResponse(this, loginResponse);
        }

        public event EventHandler<LoginResponse> NewResponse;
    }
}
