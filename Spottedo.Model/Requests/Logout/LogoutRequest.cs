﻿using System;
using Newtonsoft.Json;
using Spottedo.Model.Responses;
using Spottedo.Model.Responses.Logout;

namespace Spottedo.Model.Requests.Logout
{
    public class LogoutRequest : BaseRequest
    {
        public async override void SendAsync()
        {
            var body = "=" + JsonConvert.SerializeObject(this);
            var response = await SendHelper(@"logout", body);

            var responseString = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            var logoutResponse = !String.IsNullOrEmpty(responseString) ? JsonConvert.DeserializeObject<LogoutResponse>(responseString) : new LogoutResponse();

            logoutResponse.HttpCode = response.StatusCode;
            //FireNewResponseEvent(logoutResponse);
            if (NewResponse != null)
                NewResponse(this, logoutResponse);
        }

        public event EventHandler<LogoutResponse> NewResponse;
    }
}
