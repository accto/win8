﻿namespace Spottedo.Model.Requests.Message
{
    using System;
    using Common.DTO;
    using Navigation;
    using Newtonsoft.Json;
    using Responses.Message;

    public class SendMessageRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }

        [JsonProperty(PropertyName = "content")]
        public byte[] Content;

        [JsonProperty(PropertyName = "location")]
        public Location Location { get; private set; }

        public SendMessageRequest(int? parentId, string msg, byte[] content)
        {
            Content = content;
            ParentId = parentId;
            Msg = msg;

            //TODO: Dorobić narzędzie geolokalizacyjne
            Location = new Location(0, 0, 0, LocationSourceEnum.Gps);
        }

        public async override void SendAsync()
        {
            var body = JsonConvert.SerializeObject(this);
            var response = await SendHelper("message", body);

            var responseString = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            var messageResponse = !String.IsNullOrEmpty(responseString) ? JsonConvert.DeserializeObject<MessageSendResponse>(responseString) : new MessageSendResponse();
            messageResponse.HttpCode = response.StatusCode;
            if (OnResponse != null)
                OnResponse(this, messageResponse);
        }

        public event EventHandler<MessageSendResponse> OnResponse;
    }
}