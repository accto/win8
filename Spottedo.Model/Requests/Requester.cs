﻿using System;
using System.Threading;
using Spottedo.Model.Responses;

namespace Spottedo.Model.Requests
{
    public static class Requester
    {
        private static int dueTime = 3000;
        private static int period = 5000;
        private static Timer timer;

        static Requester()
        {
            timer = new Timer(TimerCallback, null, dueTime, period);
        }

        private static void TimerCallback(object state)
        {
            throw new NotImplementedException();
        }

        private static void CallBack(object o)
        {
            
        }

        static void req_NewResponse(object sender, BaseResponse e)
        {
            
        }
    }
}
