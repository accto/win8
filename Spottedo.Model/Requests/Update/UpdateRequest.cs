﻿using System;
using Newtonsoft.Json;
using Spottedo.Model.Navigation;
using Spottedo.Model.Responses.Update;

namespace Spottedo.Model.Requests.Update
{
    using System.Collections.Generic;
    using Messages;

    public class UpdateRequest : BaseRequest
    {
        public int Since { get; set; }
        public Location Location { get; set; }
        public List<Message> IgnoreMessages { get; set; }
        public int StartFrom { get; set; }
        public int Count { get; set; }

        public async override void SendAsync()
        {
            var body = JsonConvert.SerializeObject(this);
            var response = await SendHelper(@"update", body);

            var responseString = (await response.Content.ReadAsStringAsync()).Replace("\"", "");
            var updateResponse = !String.IsNullOrEmpty(responseString) ? JsonConvert.DeserializeObject<UpdateResponse>(responseString) : new UpdateResponse();

            updateResponse.HttpCode = response.StatusCode;
            if (NewResponse != null)
                NewResponse(this, updateResponse);
        }

        public event EventHandler<UpdateResponse> NewResponse;
    }
}