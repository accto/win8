﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Spottedo.Model.Requests.Update
{
    using Common.DTO;
    using Common.Utils;
    using Messages;
    using Navigation;
    using Responses.Update;

    public static class UpdateRequester
    {
        private static int dueTime = 3000;
        private static int period = 5000;
        private static Timer timer;
        public static bool IsUpdate { get; set; }

        public static event Action<UpdateResponse> ResponseEvent;

        static UpdateRequester()
        {
            timer = new Timer(TimerCallback, null, dueTime, period);
            IsUpdate = false;
        }

        private static void TimerCallback(object state)
        {
            if (IsUpdate)
            {
                //TODO: Z parametryzować zapytanie.
                var request = new UpdateRequest
                {
                    Since = Tools.ConvertToEpoch(DateTime.Now.AddMilliseconds(-period)),
                    Location = new Location(0, 0, 0, LocationSourceEnum.Gps),
                    IgnoreMessages = new List<Message>(),
                    StartFrom = 0,
                    Count = 10
                };
                request.NewResponse += (sender, response) => ResponseEvent(response);
                request.SendAsync();
            }
        }
    }
}