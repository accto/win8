﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Spottedo.Model.Requests
{
    using System.Linq;

    public abstract class BaseRequest
    {
        private static CookieCollection cookies = new CookieCollection();
        public const string ApiAddress = @"http://localhost:49998/api/";
        //public const string ApiAddress = @"http://restsrv.azurewebsites.net/api/";

        public static string ContentType
        {
            get { return @"application/x-www-form-urlencoded"; }
        }

        protected async static Task<HttpResponseMessage> SendHelper(string function, string body)
        {
            var cookiesContainer = new CookieContainer();
            cookiesContainer.Add(new Uri(ApiAddress), Cookies);
            var handler = new HttpClientHandler{CookieContainer = cookiesContainer, UseCookies = true};
            using (var client = new HttpClient(handler))
            {
                var contentPost = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("", body)
                });

                var address = ApiAddress + function;
                var response = await client.PostAsync(address, contentPost);

                var newCookies = cookiesContainer.GetCookies(new Uri(ApiAddress)).Cast<Cookie>();
                foreach (var cookie in newCookies)
                {
                    Cookies.Add(cookie);
                }
                
                return response;
            }
        }

        public static CookieCollection Cookies
        {
            get { return cookies; }
            private set { cookies = value; }
        }

        public abstract void SendAsync();


    }
}
