﻿using Newtonsoft.Json;

namespace Spottedo.Model.Messages
{
    using Common.DTO;

    public class Message
    {
        [JsonProperty(PropertyName = "messageId")]
        public int MessageId { get; set; }

        [JsonProperty(PropertyName = "parentId")]
        public int? ParentId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public MessageTypeEnum Type { get; set; }

        [JsonProperty(PropertyName = "nick")]
        public string Nick { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Msg { get; set; }

        [JsonProperty(PropertyName = "content")]
        public byte[] Content { get; set; }

        [JsonProperty(PropertyName = "distance")]
        public decimal Distance { get; set; }

        [JsonProperty(PropertyName = "sourceOs")]
        public OsEnum SourceOs { get; set; }

        [JsonProperty(PropertyName = "localizationSource")]
        public LocationSourceEnum LocalizationSource { get; set; }
    }
}
