﻿namespace Spottedo.Model.Responses.Message
{
    public class MessageSendResponse : BaseResponse
    {
        public int Id { get; set; }
    }
}
