﻿namespace Spottedo.Model.Responses.Login
{
    public class LoginResponse : BaseResponse
    {
//        public HttpStatusCode HttpCode { get; set; }
//        public int ServerTime { get; set; }
        public string Version { get; set; }
        public bool IsLogRcv { get; set; }
        public string Msg { get; set; }
        public string NewLogin { get; set; }
    }
}
