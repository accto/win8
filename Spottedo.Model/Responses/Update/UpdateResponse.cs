﻿using System.Collections.Generic;

namespace Spottedo.Model.Responses.Update
{
    using Messages;

    public class UpdateResponse : BaseResponse
    {
        public List<Message> Messages { get; set; }
    }
}
