﻿using System.Net;

namespace Spottedo.Model.Responses
{
    public abstract class BaseResponse
    {
        public HttpStatusCode HttpCode { get; set; }
        public int ServerTime { get; set; }
    }
}
