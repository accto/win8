﻿namespace Spottedo.ViewModel.Message
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Model.Requests.Update;
    using Model.Responses.Update;

    public class MessageContainer : IEnumerable<MessageVM>
    {
        //TODO: dorobić odporność na wielowątkowość
        private static MessageContainer instance = new MessageContainer();

        private readonly List<MessageVM> messagesVms = new List<MessageVM>();

        public static MessageContainer Instance
        {
            get { return instance; }
        }

        private MessageContainer()
        {
            UpdateRequester.ResponseEvent += UpdateRequesterOnResponse;
        }

        private void UpdateRequesterOnResponse(UpdateResponse updateResponse)
        {
            if (updateResponse.Messages != null)
            {
                var newMessages = updateResponse.Messages.Select(message => new MessageVM
                {
                    Id = message.MessageId,
                    ParentId = message.ParentId,
                    Nick = message.Nick,
                    Response = message.Msg,
                    DistanceString = string.Format("{0} m", message.Distance.ToString()),
                    LocalizationSource = message.LocalizationSource,
                    Time = new DateTime(2014, 06, 20, 12, 0, 0) //TODO: Poprawić, gdy w  protokole
                }).ToList();

                var uniqueMessages = newMessages.Where(a => this.All(b => a.Id != b.Id));
                var messageVmsNotNull = uniqueMessages as MessageVM[] ?? uniqueMessages.ToArray();
                messagesVms.AddRange(messageVmsNotNull);
                NewMessages(messageVmsNotNull);
            }
        }

        public event Action<IEnumerable<MessageVM>> NewMessages;

        public IEnumerator<MessageVM> GetEnumerator()
        {
            return messagesVms.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}