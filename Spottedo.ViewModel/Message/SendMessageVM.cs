﻿namespace Spottedo.ViewModel.Message
{
    using System;
    using Model.Navigation;
    using Model.Requests.Message;

    public class SendMessageVM
    {
        public int? ParentId { get; set; }
        public string Msg { get; set; }
        public byte[] Content { get; set; }

        public static event Action MessageDelivered;

        public void Send()
        {
            var sendModel = new SendMessageRequest(ParentId, Msg, Content);
            sendModel.OnResponse += (sender, response) =>
            {
                if (MessageDelivered != null)
                    MessageDelivered();
            };
            sendModel.SendAsync();
        }
    }
}