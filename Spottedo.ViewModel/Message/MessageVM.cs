﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Spottedo.ViewModel.Annotations;

namespace Spottedo.ViewModel.Message
{
    using System;
    using Common.DTO;

    public class MessageVM : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        private string distanceString;
        public DateTime Time { get; set; }
        public string Nick { get; set; }
        public string Response { get; set; }
        public string Content { get; set; }
        //private OsEnum SourceOs { get; set; }
        public LocationSourceEnum LocalizationSource { get; set; }

        public string DistanceString
        {
            get { return distanceString; }
            set
            {
                if (value == distanceString)
                    return;
                distanceString = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}