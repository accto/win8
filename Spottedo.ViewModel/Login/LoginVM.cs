﻿using System.ComponentModel;
using System.Net;
using Spottedo.Model.Requests.Login;
using Spottedo.Model.Responses.Login;

namespace Spottedo.ViewModel.Login
{
    using Common.DTO;
    using Model.Requests.Update;

    public class LoginVM : INotifyPropertyChanged
    {
        private bool _isLoginControlVisible;

        public string Login { get; set; }

        public string Password { get; set; }

        public bool IsLoginControlVisible
        {
            get { return _isLoginControlVisible; }
            private set
            {
                PropertyChanged(this, new PropertyChangedEventArgs("IsLoginControlVisible"));
                _isLoginControlVisible = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void TryLogin()
        {
            var lr = MapToLoginRequest();
            lr.NewResponse += OnLoginResponseOnNewResponse;
            lr.SendAsync();
        }

        private void OnLoginResponseOnNewResponse(object sender, LoginResponse response)
        {
            if (response.HttpCode == HttpStatusCode.OK)
            {
                IsLoginControlVisible = false;
                UpdateRequester.IsUpdate = true;
            }
        }

        private LoginRequest MapToLoginRequest()
        {
            return new LoginRequest
            {
                //TODO: Pobiereać z AppConfig
                Os = OsEnum.Wm,
                Locale = "pl",
                Version = "1.0",
                Login = this.Login,
                Password = this.Password
            };
        }
    }
}