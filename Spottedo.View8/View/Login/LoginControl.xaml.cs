﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Spottedo.View8.View.Login
{
    using System.ComponentModel;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using ViewModel.Login;

    public sealed partial class LoginControl : UserControl
    {
        public LoginControl()
        {
            this.InitializeComponent();
            var loginRequest = new LoginVM();
            loginRequest.PropertyChanged += LoginRequestOnPropertyChanged;
            this.DataContext = loginRequest;
        }

        private void LoginRequestOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "IsLoginControlVisible")
            {
                var context = (LoginVM) DataContext;
                Visibility = context.IsLoginControlVisible ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var loginVM = (LoginVM)DataContext;
            //loginRequestVM.LoginEvent += OnLoginRequestVmOnLoginEvent;
            loginVM.TryLogin();
        }

        //private void OnLoginRequestVmOnLoginEvent(object o, LoginResponseVM response)
        //{
        //    var status = response.HttpCode;
        //    if (status == HttpStatusCode.OK)
        //        OnResponse(this, response);
        //}

        //public event EventHandler<LoginResponseVM> OnResponse;
    }
}
