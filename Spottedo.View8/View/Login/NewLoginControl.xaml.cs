﻿using System;
using System.Net;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using Spottedo.ViewModel.Login;

namespace Spottedo.View.View.Login
{
    public sealed partial class NewLoginControl : UserControl
    {
        public NewLoginControl()
        {
            this.InitializeComponent();
            this.LoginStackPanel.DataContext = new LoginRequestVM();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var loginRequestVM = (LoginRequestVM)LoginStackPanel.DataContext;
            loginRequestVM.LoginEvent += (o, response) =>
            {
                var status = response.HttpCode;
                if (OnResponse != null)
                    OnResponse(this, response);
            };
            loginRequestVM.TryLogin();
        }

        public event EventHandler<LoginResponseVM> OnResponse;
    }
}
