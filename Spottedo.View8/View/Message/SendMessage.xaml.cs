﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Spottedo.View8.View.Message
{
    using ViewModel.Message;

    public sealed partial class SendMessage : UserControl
    {
        public SendMessage()
        {
            this.InitializeComponent();
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            var sendMessage = (SendMessageVM) this.DataContext;
            sendMessage.Send();
        }
    }
}
