﻿using MetroLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spottedo.Common.Loggers
{
    class BasicLogger : ILogger
    {
        public void Debug(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Debug(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Info(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Info(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public bool IsDebugEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsEnabled(LogLevel level)
        {
            throw new NotImplementedException();
        }

        public bool IsErrorEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsFatalEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInfoEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsTraceEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsWarnEnabled
        {
            get { throw new NotImplementedException(); }
        }

        public void Log(LogLevel logLevel, string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Log(LogLevel logLevel, string message, Exception ex)
        {
            throw new NotImplementedException();
        }

        public string Name
        {
            get { throw new NotImplementedException(); }
        }

        public void Trace(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Trace(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }
    }
}
