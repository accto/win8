﻿using System;
using Windows.ApplicationModel.Email;
using MetroLog;

namespace Spottedo.DTO.Log
{
    internal class MailLog : ILogger
    {
        public void Trace(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Trace(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Debug(string message, Exception ex = null)
        {
            var sendTo = new EmailRecipient("mibalogin@gmail.com");
            var mail = new EmailMessage
            {
                Subject = message,
                Body = ex != null ? ex.ToString() : ""
            };
            mail.To.Add(sendTo);
            EmailManager.ShowComposeNewEmailAsync(mail);
        }

        public void Debug(string message, params object[] ps)
        {
            var sendTo = new EmailRecipient("mibalogin@gmail.com");
            var mail = new EmailMessage
            {
                Body = string.Format(message, ps)
            };
            mail.To.Add(sendTo);
            EmailManager.ShowComposeNewEmailAsync(mail);
        }

        public void Info(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Info(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, Exception ex = null)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public void Log(LogLevel logLevel, string message, Exception ex)
        {
            throw new NotImplementedException();
        }

        public void Log(LogLevel logLevel, string message, params object[] ps)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel level)
        {
            throw new NotImplementedException();
        }

        public string Name { get; private set; }
        public bool IsTraceEnabled { get; private set; }

        public bool IsDebugEnabled
        {
            get { return true; }
        }

        public bool IsInfoEnabled
        {
            get { return false; }
        }

        public bool IsWarnEnabled
        {
            get { return false; }
        }

        public bool IsErrorEnabled
        {
            get { return false; }
        }

        public bool IsFatalEnabled
        {
            get { return false; }
        }
    }
}