﻿namespace Spottedo.Common.DTO
{
    using Newtonsoft.Json;

    public enum LocationSourceEnum
    {
        [JsonProperty(PropertyName = "GPS")]
        Gps,
        [JsonProperty(PropertyName = "GEOLOCATION")]
        Geolocation,
        [JsonProperty(PropertyName = "MAP")]
        Map
    }
}