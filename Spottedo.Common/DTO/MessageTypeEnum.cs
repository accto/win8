﻿namespace Spottedo.Common.DTO
{
    using Newtonsoft.Json;

    public enum MessageTypeEnum
    {
        [JsonProperty(PropertyName = "MSG")]
        Msg,
        [JsonProperty(PropertyName = "NOTICE")]
        Notice
    }
}